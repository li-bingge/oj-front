import { RouteRecordRaw } from "vue-router";
import About from "@/views/About.vue";
import NoAuth from "@/views/NoAuth.vue";
import ACCESS_ENUM from "@/access/accessEnum";
import UserLayout from "@/layouts/UserLayout.vue";
import UserLoginView from "@/views/user/UserLoginView.vue";
import UserRegisterView from "@/views/user/UserRegisterView.vue";
import QuestionAddView from "@/views/question/QuestionAddView.vue";
import QuestionManageView from "@/views/question/QuestionManageView.vue";
import QuestionsView from "@/views/question/QuestionsView.vue";
import QuestionViewView from "@/views/question/QuestionViewView.vue";

export const routes: Array<RouteRecordRaw> = [
  {
    path: "/user",
    name: "用户",
    component: UserLayout,
    children: [
      { path: "/user/login", name: "用户登录", component: UserLoginView },
      { path: "/user/register", name: "用户注册", component: UserRegisterView },
    ],
    meta: {
      hideInMenu: true,
    },
  },

  {
    path: "/noAuth",
    name: "没有权限",
    component: NoAuth,
    meta: {
      hideInMenu: true,
    },
  },
  // {
  //   path: "/admin",
  //   name: "管理员可见",
  //   component: Admin,
  //   meta: {
  //     access: ACCESS_ENUM.ADMIN,
  //   },
  // },

  {
    path: "/about",
    name: "关于我的",
    component: About,
    meta: {
      hideInMenu: true,
    },
  },
  {
    path: "/question/add",
    name: "创建题目",
    component: QuestionAddView,
    meta: {
      access: ACCESS_ENUM.ADMIN,
    },
  },
  {
    path: "/question/manage",
    name: "管理题目",
    component: QuestionManageView,
    meta: {
      access: ACCESS_ENUM.ADMIN,
    },
  },
  {
    path: "/question/update",
    name: "更新题目",
    component: QuestionAddView,
    meta: {
      access: ACCESS_ENUM.ADMIN,
      hideInMenu: true,
    },
  },
  {
    path: "/",
    name: "主页",
    component: QuestionsView,
  },
  {
    path: "/questions",
    name: "浏览题目",
    component: QuestionsView,
  },
  {
    path: "/question/view/:id",
    name: "在线做题",
    component: QuestionViewView,
    props: true,
    meta: {
      access: ACCESS_ENUM.USER,
    },
  },
  // {
  //   path: "/hinder",
  //   name: "隐藏界面",
  //   component: Hinder,
  //   meta: {
  //     hideInMenu: true,
  //   },
  // },
];
