import { createStore } from "vuex";
import user from "./user";

export default createStore({
  mutations: {},
  actions: {},
  modules: {
    // 进行启用user.ts的东西
    user,
  },
});
